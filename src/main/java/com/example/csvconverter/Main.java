package com.example.csvconverter;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class Main {
    public static void main(String[] args) {
        SpringApplication.run(CsvconverterApplication.class, args);
        CsvconverterApplication app = new CsvconverterApplication();
    }
}
