package com.example.csvconverter.model;

public class bikeModel {

    private String numericModelID;
    private String marketingModelNumber;
    private String modelYear;
    private String modelName;
    private String textIdentifier;
    private String travel;
    private String frontSuspensionName;
    private String rearSuspensionName;
    private frontSuspension frontSuspension;
    private rearSuspension rearSuspension;

    public bikeModel() {
    }

    public bikeModel(String numericModelID, String marketingModelNumber, String modelYear, String modelName, String textIdentifier, String travel, String frontSuspensionName, String rearSuspensionName, com.example.csvconverter.model.frontSuspension frontSuspension, com.example.csvconverter.model.rearSuspension rearSuspension) {
        this.numericModelID = numericModelID;
        this.marketingModelNumber = marketingModelNumber;
        this.modelYear = modelYear;
        this.modelName = modelName;
        this.textIdentifier = textIdentifier;
        this.travel = travel;
        this.frontSuspensionName = frontSuspensionName;
        this.rearSuspensionName = rearSuspensionName;
        this.frontSuspension = frontSuspension;
        this.rearSuspension = rearSuspension;
    }

    public String getNumericModelID() {
        return numericModelID;
    }

    public void setNumericModelID(String numericModelID) {
        this.numericModelID = numericModelID;
    }

    public String getMarketingModelNumber() {
        return marketingModelNumber;
    }

    public void setMarketingModelNumber(String marketingModelNumber) {
        this.marketingModelNumber = marketingModelNumber;
    }

    public String getModelYear() {
        return modelYear;
    }

    public void setModelYear(String modelYear) {
        this.modelYear = modelYear;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getTextIdentifier() {
        return textIdentifier;
    }

    public void setTextIdentifier(String textIdentifier) {
        this.textIdentifier = textIdentifier;
    }

    public String getTravel() {
        return travel;
    }

    public void setTravel(String travel) {
        this.travel = travel;
    }

    public String getFrontSuspensionName() {
        return frontSuspensionName;
    }

    public void setFrontSuspensionName(String frontSuspensionName) {
        this.frontSuspensionName = frontSuspensionName;
    }

    public String getRearSuspensionName() {
        return rearSuspensionName;
    }

    public void setRearSuspensionName(String rearSuspensionName) {
        this.rearSuspensionName = rearSuspensionName;
    }

    public com.example.csvconverter.model.frontSuspension getFrontSuspension() {
        return frontSuspension;
    }

    public void setFrontSuspension(com.example.csvconverter.model.frontSuspension frontSuspension) {
        this.frontSuspension = frontSuspension;
    }

    public com.example.csvconverter.model.rearSuspension getRearSuspension() {
        return rearSuspension;
    }

    public void setRearSuspension(com.example.csvconverter.model.rearSuspension rearSuspension) {
        this.rearSuspension = rearSuspension;
    }

    @Override
    public String toString() {
        return "bikeModel{" +
                "numericModelID='" + numericModelID + '\'' +
                ", marketingModelNumber='" + marketingModelNumber + '\'' +
                ", modelYear='" + modelYear + '\'' +
                ", modelName='" + modelName + '\'' +
                ", textIdentifier='" + textIdentifier + '\'' +
                ", travel='" + travel + '\'' +
                ", frontSuspensionName='" + frontSuspensionName + '\'' +
                ", rearSuspensionName='" + rearSuspensionName + '\'' +
                ", frontSuspension=" + frontSuspension +
                ", rearSuspension=" + rearSuspension +
                '}';
    }
}