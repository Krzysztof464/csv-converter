package com.example.csvconverter.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class rearSuspension {

    private List<String> rearSpring;
    private List<String> rearRebound;
    private List<String> rearShockStroke;
    private List<String> rearShockSag;

    public rearSuspension() {
        this.rearSpring = new ArrayList<String>();
        this.rearRebound = new ArrayList<String>();
        this.rearShockStroke = new ArrayList<String>();
        this.rearShockSag = new ArrayList<String>();
    }

    public rearSuspension(List<String> rearSpring, List<String> rearRebound, List<String> rearShockStroke, List<String> rearShockSag) {
        this.rearSpring = rearSpring;
        this.rearRebound = rearRebound;
        this.rearShockStroke = rearShockStroke;
        this.rearShockSag = rearShockSag;
    }

    public List<String> getRearSpring() {
        return rearSpring;
    }

    public void setRearSpring(List<String> rearSpring) {
        this.rearSpring = rearSpring;
    }

    public List<String> getRearRebound() {
        return rearRebound;
    }

    public void setRearRebound(List<String> rearRebound) {
        this.rearRebound = rearRebound;
    }

    public List<String> getRearShockStroke() {
        return rearShockStroke;
    }

    public void setRearShockStroke(List<String> rearShockStroke) {
        this.rearShockStroke = rearShockStroke;
    }

    public List<String> getRearShockSag() {
        return rearShockSag;
    }

    public void setRearShockSag(List<String> rearShockSag) {
        this.rearShockSag = rearShockSag;
    }

    @Override
    public String toString() {
        return "rearSuspension{" +
                "rearSpring=" + rearSpring +
                ", rearRebound=" + rearRebound +
                ", rearShockStroke=" + rearShockStroke +
                ", rearShockSag=" + rearShockSag +
                '}';
    }
}
