package com.example.csvconverter.model;

import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

public class frontSuspension {

    private List<String> frontSpring;
    private List<String> frontRebound;
    private List<String> frontForkSag;

    public frontSuspension() {
        this.frontSpring = new ArrayList<String>();
        this.frontRebound = new ArrayList<String>();
        this.frontForkSag = new ArrayList<String>();
    }

    public frontSuspension(List<String> frontSpring, List<String> frontRebound, List<String> frontForkSag) {
        this.frontSpring = frontSpring;
        this.frontRebound = frontRebound;
        this.frontForkSag = frontForkSag;
    }

    public List<String> getFrontSpring() {
        return frontSpring;
    }

    public void setFrontSpring(List<String> frontSpring) {
        this.frontSpring = frontSpring;
    }

    public List<String> getFrontRebound() {
        return frontRebound;
    }

    public void setFrontRebound(List<String> frontRebound) {
        this.frontRebound = frontRebound;
    }

    public List<String> getFrontForkSag() {
        return frontForkSag;
    }

    public void setFrontForkSag(List<String> frontForkSag) {
        this.frontForkSag = frontForkSag;
    }

    @Override
    public String toString() {
        return "frontSuspension{" +
                "frontSpring=" + frontSpring +
                ", frontRebound=" + frontRebound +
                ", frontForkSag=" + frontForkSag +
                '}';
    }
}
