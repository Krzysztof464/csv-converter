package com.example.csvconverter.dataconversion;

import com.example.csvconverter.model.bikeModel;
import com.example.csvconverter.model.frontSuspension;
import com.example.csvconverter.model.rearSuspension;

import java.io.BufferedReader;
import java.io.FileNotFoundException;
import java.io.FileReader;
import java.util.ArrayList;
import java.util.List;

public class dataReader {

    ClassLoader classLoader = getClass().getClassLoader();
    private BufferedReader csvReader;

    {
        try {
            csvReader = new BufferedReader(new FileReader(classLoader.getResource("input.csv").getFile()));
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
    }

    public List<bikeModel> readCSV() {

        List<bikeModel> bikeModels = new ArrayList<bikeModel>();
        String row;

        try {
            while ((row = csvReader.readLine()) != null) {
                String[] data = row.split(",");
                if (data.length > 0 && data[0].equals("BIKE MODEL")) {
                    bikeModels.add(readBikeModelFromCsv());
                }
            }
        }
        catch (Exception e) {e.printStackTrace();}

        return bikeModels;
    }

    public bikeModel readBikeModelFromCsv() {

        bikeModel bikeModel = new bikeModel();
        String row;

        try {
            while ((row = csvReader.readLine()) != null) {
                String[] data = row.split(",(?=([^\"]*\"[^\"]*\")*[^\"]*$)");
                if (data.length > 0) {
                    switch (data[0]) {
                        case "Numeric Model ID":
                            bikeModel.setNumericModelID(data[1]);
                            break;
                        case "Marketing Model Number":
                            bikeModel.setMarketingModelNumber(data[1]);
                            break;
                        case "Model Year":
                            bikeModel.setModelYear(data[1]);
                            break;
                        case "Model Name":
                            bikeModel.setModelName(data[1]);
                            break;
                        case "Text Identifier":
                            if (data.length > 1) {
                                bikeModel.setTextIdentifier(data[1]);
                            } else {
                                bikeModel.setTextIdentifier("");
                            }
                            break;
                        case "Travel":
                            bikeModel.setTravel(data[1]);
                            break;
                        case "Front Suspension":
                            bikeModel.setFrontSuspensionName(data[1]);
                            System.out.printf(data[1]);
                            bikeModel.setFrontSuspension(readFrontSuspensionFromCsv());
                            break;
                        case "Rear Suspension":
                            bikeModel.setRearSuspensionName(data[1]);
                            bikeModel.setRearSuspension(readRearSuspensionFromCsv());
                            break;
                        case "END BIKE MODEL":
                            return bikeModel;
                    }
                }
            }
        }
        catch (Exception e) {e.printStackTrace();}

        return null;
    }

    public frontSuspension readFrontSuspensionFromCsv() {

        List<String> frontSpring = new ArrayList<String>();
        List<String> frontRebound = new ArrayList<String>();
        List<String> frontForkSag = new ArrayList<String>();

        String row;

        try {
            csvReader.readLine();
            for (int i = 0; i < 16; i++) {
                row = csvReader.readLine();
                String[] data = row.split(",");
                frontSpring.add(data[1]);
                frontRebound.add(data[3]);
                frontForkSag.add(data[5]);
            }
        }
        catch (Exception e) {e.printStackTrace();}

        return new frontSuspension(frontSpring, frontRebound, frontForkSag);
    }

    public rearSuspension readRearSuspensionFromCsv() {

        List<String> rearSpring = new ArrayList<>();
        List<String> rearRebound = new ArrayList<>();
        List<String> rearShockStroke = new ArrayList<>();
        List<String> rearShockSag = new ArrayList<>();

        String row;

        try {
            csvReader.readLine();
            for (int i = 0; i < 16; i++) {
                row = csvReader.readLine();
                String[] data = row.split(",");
                rearSpring.add(data[1]);
                rearRebound.add(data[3]);
                rearShockStroke.add(data[5]);
                rearShockSag.add(data[7]);
            }
        }
        catch (Exception e) {e.printStackTrace();}

        return new rearSuspension(rearSpring, rearRebound, rearShockStroke, rearShockSag);
    }
}
