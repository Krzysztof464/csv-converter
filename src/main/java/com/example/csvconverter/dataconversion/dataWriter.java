package com.example.csvconverter.dataconversion;

import com.example.csvconverter.model.bikeModel;

import java.io.File;
import java.io.FileWriter;
import java.io.IOException;
import java.util.List;

public class dataWriter {

    public boolean createCSVFile () {
        File file = new File("output.csv");
        try {
            return file.createNewFile();
        } catch (IOException e) {
            e.printStackTrace();
        }
        return false;
    }

    public void writeCSVFile (List<bikeModel> bikeModels) {

        try {
            FileWriter fileWriter = new FileWriter("output.csv");
            fileWriter.write("Numeric Model ID,Marketing Model Number,Model Year,Model Name,Text Identifier,Travel,Front Suspension,Rear Suspension,Front Spring 0-100,Front Spring 101-110,Front Spring 111-120,Front Spring 121-130,Front Spring 131-140,Front Spring 141-150,Front Spring 151-160,Front Spring 161-170,Front Spring 171-180,Front Spring 181-190,Front Spring 191-200,Front Spring 201-210,Front Spring 211-220,Front Spring 221-230,Front Spring 231-240,Front Spring 241-250,Front Rebound 0-100,Front Rebound 101-110,Front Rebound 111-120,Front Rebound 121-130,Front Rebound 131-140,Front Rebound 141-150,Front Rebound 151-160,Front Rebound 161-170,Front Rebound 171-180,Front Rebound 181-190,Front Rebound 191-200,Front Rebound 201-210,Front Rebound 211-220,Front Rebound 221-230,Front Rebound 231-240,Front Rebound 241-250,Front Fork Sag 0-100,Front Fork Sag 101-110,Front Fork Sag 111-120,Front Fork Sag 121-130,Front Fork Sag 131-140,Front Fork Sag 141-150,Front Fork Sag 151-160,Front Fork Sag 161-170,Front Fork Sag 171-180,Front Fork Sag 181-190,Front Fork Sag 191-200,Front Fork Sag 201-210,Front Fork Sag 211-220,Front Fork Sag 221-230,Front Fork Sag 231-240,Front Fork Sag 241-250,Rear Spring 0-100,Rear Spring 101-110,Rear Spring 111-120,Rear Spring 121-130,Rear Spring 131-140,Rear Spring 141-150,Rear Spring 151-160,Rear Spring 161-170,Rear Spring 171-180,Rear Spring 181-190,Rear Spring 191-200,Rear Spring 201-210,Rear Spring 211-220,Rear Spring 221-230,Rear Spring 231-240,Rear Spring 241-250,Rear Rebound 0-100,Rear Rebound 101-110,Rear Rebound 111-120,Rear Rebound 121-130,Rear Rebound 131-140,Rear Rebound 141-150,Rear Rebound 151-160,Rear Rebound 161-170,Rear Rebound 171-180,Rear Rebound 181-190,Rear Rebound 191-200,Rear Rebound 201-210,Rear Rebound 211-220,Rear Rebound 221-230,Rear Rebound 231-240,Rear Rebound 241-250,Rear Shock Stroke 0-100,Rear Shock Stroke 101-110,Rear Shock Stroke 111-120,Rear Shock Stroke 121-130,Rear Shock Stroke 131-140,Rear Shock Stroke 141-150,Rear Shock Stroke 151-160,Rear Shock Stroke 161-170,Rear Shock Stroke 171-180,Rear Shock Stroke 181-190,Rear Shock Stroke 191-200,Rear Shock Stroke 201-210,Rear Shock Stroke 211-220,Rear Shock Stroke 221-230,Rear Shock Stroke 231-240,Rear Shock Stroke 241-250,Rear Shock Sag PSI,Rear Shock Sag PSI,Rear Shock Sag PSI,Rear Shock Sag PSI,Rear Shock Sag PSI,Rear Shock Sag PSI,Rear Shock Sag PSI,Rear Shock Sag PSI,Rear Shock Sag PSI,Rear Shock Sag PSI,Rear Shock Sag PSI,Rear Shock Sag PSI,Rear Shock Sag PSI,Rear Shock Sag PSI,Rear Shock Sag PSI,Rear Shock Sag PSI,\n");
            for (bikeModel b : bikeModels) {
                fileWriter.write(b.getNumericModelID()+","+b.getMarketingModelNumber()+","+b.getModelYear()+","+b.getModelName()+","+b.getTextIdentifier()+","+b.getTravel()+","+b.getFrontSuspensionName()+","+b.getRearSuspensionName()+",");
                for (int i = 0; i < b.getFrontSuspension().getFrontSpring().size(); i++) {
                    fileWriter.write(b.getFrontSuspension().getFrontSpring().get(i)+" PSI,");
                }
                for (int i = 0; i < b.getFrontSuspension().getFrontRebound().size(); i++) {
                    fileWriter.write(b.getFrontSuspension().getFrontRebound().get(i)+" clicks out,");
                }
                for (int i = 0; i < b.getFrontSuspension().getFrontForkSag().size(); i++) {
                    fileWriter.write(b.getFrontSuspension().getFrontForkSag().get(i)+" mm,");
                }
                for (int i = 0; i < b.getRearSuspension().getRearSpring().size(); i++) {
                    fileWriter.write(b.getRearSuspension().getRearSpring().get(i)+" PSI,");
                }
                for (int i = 0; i < b.getRearSuspension().getRearRebound().size(); i++) {
                    fileWriter.write(b.getRearSuspension().getRearRebound().get(i)+" clicks out,");
                }
                for (int i = 0; i < b.getRearSuspension().getRearShockStroke().size(); i++) {
                    fileWriter.write(b.getRearSuspension().getRearShockStroke().get(i)+" mm,");
                }
                for (int i = 0; i < b.getRearSuspension().getRearShockSag().size(); i++) {
                    fileWriter.write(b.getRearSuspension().getRearShockSag().get(i)+" mm,");
                }
                fileWriter.write("\n");
            }
            fileWriter.close();
        } catch (IOException e) {
            e.printStackTrace();
        }
    }
}
