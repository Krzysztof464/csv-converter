package com.example.csvconverter;

import com.example.csvconverter.dataconversion.dataWriter;
import com.example.csvconverter.dataconversion.dataReader;
import com.example.csvconverter.model.bikeModel;

import java.util.List;

public class CsvconverterApplication {

	public CsvconverterApplication() {
			dataReader dataReader = new dataReader();
			List<bikeModel> bikeModels = dataReader.readCSV();

			dataWriter dataWriter = new dataWriter();
			dataWriter.createCSVFile();
			dataWriter.writeCSVFile(bikeModels);
	}
}
